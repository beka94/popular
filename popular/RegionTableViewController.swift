//
//  RegionTableViewController.swift
//  popular
//
//  Created by beka on 4/6/17.
//  Copyright © 2017 beka. All rights reserved.
//

import UIKit
import SwiftyJSON

class RegionTableViewController: UITableViewController {
    
    var message_oblast=JSON.null
    var message_city=JSON.null
    var message_selo=JSON.null
    var params=[String:String]()
    var loadingView:UIView={
        let l=UIView()
        l.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive=true
        l.backgroundColor=UIColor.black.withAlphaComponent(0.4)
        l.translatesAutoresizingMaskIntoConstraints=false
        let activity=UIActivityIndicatorView()
        activity.startAnimating()
        activity.activityIndicatorViewStyle = .whiteLarge
        activity.translatesAutoresizingMaskIntoConstraints=false
        l.addSubview(activity)
        activity.centerXAnchor.constraint(equalTo: l.centerXAnchor).isActive=true
        activity.centerYAnchor.constraint(equalTo: l.centerYAnchor).isActive=true
        l.isHidden=true
        return l
    }()
    var loadingViewHeght:NSLayoutConstraint?
    var is_open1=true
    var is_open2=false
    var is_open3=false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(loadingView)
        loadingViewHeght=NSLayoutConstraint(item: loadingView, attribute:NSLayoutAttribute.height, relatedBy:NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: UIScreen.main.bounds.height)
        loadingView.addConstraint(loadingViewHeght!)
        loadingView.anchorWithConstantsToTop(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        params["method"]="get"
        params["url"]="/api/List/Oblast"
        load_oblast(params: params)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func load_oblast(params: [String : String]){
        loadingView.isHidden=false
        CustomRequests.request(params: params){
            response in
            print(response)
            self.loadingView.isHidden=true
            if response.result.error == nil{
                let result=JSON(response.result.value!)
                if result != JSON.null{
                    self.message_oblast=result
                    self.tableView.reloadData()
                }
            }else{
                let cancel=UIAlertAction(title: "Закрыть", style: .destructive){
                    (result:UIAlertAction)->Void in
                    
                }
                self.showAlert(msg: "Произошла ошибка. Повторите попытку", actions: [cancel])
            }
        }
    }
    
    func load_city(params: [String : String]){
        loadingView.isHidden=false
        CustomRequests.request(params: params){
            response in
            self.loadingView.isHidden=true
            if response.result.error == nil{
                let result=JSON(response.result.value!)
                if result != JSON.null{
                    self.message_city=result
                    self.tableView.reloadData()
                }
            }else{
                let cancel=UIAlertAction(title: "Закрыть", style: .destructive){
                    (result:UIAlertAction)->Void in
                    
                }
                self.showAlert(msg: "Произошла ошибка. Повторите попытку", actions: [cancel])
            }
        }
    }
    
    func load_selo(params: [String : String]){
        loadingView.isHidden=false
        CustomRequests.request(params: params){
            response in
            self.loadingView.isHidden=true
            if response.result.error == nil{
                let result=JSON(response.result.value!)
                if result != JSON.null{
                    self.message_selo=result
                    self.tableView.reloadData()
                }
            }else{
                let cancel=UIAlertAction(title: "Закрыть", style: .destructive){
                    (result:UIAlertAction)->Void in
                    
                }
                self.showAlert(msg: "Произошла ошибка. Повторите попытку", actions: [cancel])
            }
        }
    }
    
    @IBAction func close(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        switch section {
        case 0:
            if is_open1{
                return message_oblast.count+1
            }
            return 1
        case 1:
            if is_open2{
                return message_city.count+1
            }
            return 1
        case 2:
            if is_open3{
                return message_selo.count+1
            }
            return 1
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        switch indexPath.section {
        case 0:
            if indexPath.row==0{
                cell.textLabel?.text=selected_oblast
                cell.backgroundColor=UIColor.groupTableViewBackground
            }else{
                cell.textLabel?.text=message_oblast[indexPath.row-1]["name"].stringValue
                cell.backgroundColor=UIColor.white
            }
            break
        case 1:
            if indexPath.row==0{
                cell.textLabel?.text=selected_city
                cell.backgroundColor=UIColor.groupTableViewBackground
            }else{
                cell.textLabel?.text=message_city[indexPath.row-1]["name"].stringValue
                cell.backgroundColor=UIColor.white
            }
            break
        default:
            if indexPath.row==0{
                cell.textLabel?.text=selected_selo
                cell.backgroundColor=UIColor.groupTableViewBackground
            }else{
                cell.textLabel?.text=message_selo[indexPath.row-1]["name"].stringValue
                cell.backgroundColor=UIColor.white
            }
            break
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        switch indexPath.section {
        case 0:
            if indexPath.row==0{
                is_open1=true
                is_open2=false
                is_open3=false
                tableView.reloadData()
            }else{
                is_open1=false
                is_open2=true
                is_open3=false
                selected_oblast=message_oblast[indexPath.row-1]["name"].stringValue
                oblastid=message_oblast[indexPath.row-1]["id"].stringValue
                 params["method"]="get"
                params["url"]="/api/List/Gorod?oblastid=\(oblastid)"
                load_city(params: params)
            }
            break
        case 1:
            if indexPath.row==0{
                is_open1=false
                is_open2=true
                is_open3=false
                tableView.reloadData()
            }else{
                is_open1=false
                is_open2=false
                is_open3=true
                selected_city=message_city[indexPath.row-1]["name"].stringValue
                gorodid=message_city[indexPath.row-1]["id"].stringValue
                 params["method"]="get"
                params["url"]="/api/List/Selo?gorodid=\(gorodid)"
                load_selo(params: params)
            }
            break
        default:
            if indexPath.row==0{
                is_open1=false
                is_open2=false
                is_open3=true
                tableView.reloadData()
            }else{
                selected_selo=message_selo[indexPath.row-1]["name"].stringValue
                seloid=message_selo[indexPath.row-1]["id"].stringValue
                dismiss(animated: true, completion: nil)
            }
            break
        }
        
    }
}
