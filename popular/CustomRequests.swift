import UIKit
import Foundation
import Alamofire
import SwiftyJSON

let host="http://api.popular-plus.kz"

class CustomRequests {
    
    static func request(params:[String:String],completionHandler: @escaping (DataResponse<Any>) -> ()){
        var url=host+params["url"]!
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        switch params["method"]! {
        case "post":
            var param:Parameters=[:]
            for (key,value) in params {
                if key != "method" && key != "url"{
                    param[key]=value
                }
            }
            Alamofire.request(url, method: .post, parameters: param).responseJSON{
                response in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completionHandler(response)
            }
            break
        case "upload":
            Alamofire.upload(
                multipartFormData: { multipartFormData in
                    for (key,value) in params{
                        multipartFormData.append(value.data(using: .utf8)!, withName: key)
                    }
                    let P=photos
                    for i in 0...photos.count-1{
                        let p=photos[i]
                        if p != UIImage(){
                            if let d=UIImageJPEGRepresentation(p, 1){
                              print(d)
                              multipartFormData.append(d,withName: "photo\(i+1)", mimeType: "image/jpeg")
                            }
                        }
                    }
            },
                to: url,
                method:.post,
                headers:["Authorization": "bearer \(UserDefaults.standard.getToken())"],
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            debugPrint(response)
                            completionHandler(response)
                        }
                    case .failure(let encodingError):
                        print(encodingError)
                    }
            }
            )
            break
        case "get":
            url=url+params.dict_to_string()
            print(url)
            Alamofire.request(url,headers:["Authorization": "bearer \(UserDefaults.standard.getToken())"]).responseJSON{
                response in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completionHandler(response)
            }
            break
        default:
            break
        }
    }
}
