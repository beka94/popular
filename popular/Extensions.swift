import UIKit
var kz_dict=[
    "close":"Жабу",
    "register":"Тіркелу",
    "profile":"Жеке парақ",
    "login":"Кіру",
    "setting":"Баптаулар",
    "about":"Қосымша туралы",
    "main":"Басты бет",
    "company":"Компания туралы",
    "lang":"Тіл",
    "push":"Хабарландыру",
    "add":"Кәсіп орын қосу",
    "animal":":Жануарлар",
    "flower":"Өсімдктер",
    "type_fin":"Қаржыландыру түрі",
    "organization":"Мекемелер",
    "my":"Мекемелер тізімі",
    "forget":"Забыль пароль",
    "change":"Изменить пароль",
]
var rus_dict=[
    "close":"Закрыть",
    "register":"Регистрация",
    "login":"Вход",
    "profile":"Профиль",
    "setting":"Настройки",
    "about":"О приложении",
    "main":"Главная",
    "company":"О компании",
    "lang":"Язык",
    "push":"Уведомление",
    "add":"Добавить предприятие",
    "animal":"Животноводство",
    "flower":"Растениеводство",
    "type_fin":"Вид финансирования",
    "organization":"Организации",
    "my":"Мои предприятии",
    "forget":"Забыль пароль",
    "change":"Изменить пароль",
]
extension String{
    func localize(lang:String)->String{
        switch lang {
        case "kz":
            if kz_dict[self] != nil{
                return kz_dict[self]!
            }
            return ""
        default:
            if rus_dict[self] != nil{
                return rus_dict[self]!
            }
            return ""
        }
    }
    
}

extension UIViewController{
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func showAlert(msg:String,actions:[UIAlertAction]?) {
        let alertController = UIAlertController(title: "Сообщение", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        if actions != nil{
            for item in actions! {
                alertController.addAction(item)
            }
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showLoading(){
        let loading = UIAlertController(title: "", message: "Загрузка ...", preferredStyle: UIAlertControllerStyle.alert)
        self.present(loading, animated: true, completion: nil)
    }
}

extension UINavigationController {
    
    public func presentTransparentNavigationBar() {
        navigationBar.setBackgroundImage(UIImage(), for:UIBarMetrics.default)
        navigationBar.isTranslucent = true
        navigationBar.shadowImage = UIImage()
        navigationBar.backgroundColor=UIColor.clear
        setNavigationBarHidden(false, animated:true)
    }
    
    public func hideTransparentNavigationBar() {
        setNavigationBarHidden(true, animated:false)
        navigationBar.setBackgroundImage(UINavigationBar.appearance().backgroundImage(for: UIBarMetrics.default), for:UIBarMetrics.default)
        navigationBar.isTranslucent = UINavigationBar.appearance().isTranslucent
        navigationBar.shadowImage = UINavigationBar.appearance().shadowImage
    }
    
    public func setBG2(){
        navigationBar.setBackgroundImage(UIImage(), for:UIBarMetrics.default)
        navigationBar.isTranslucent = true
        navigationBar.isOpaque=false
        navigationBar.shadowImage = UIImage()
        navigationBar.barTintColor=UIColor(colorLiteralRed: 54/255, green: 175/255, blue: 188/255, alpha: 1)
        navigationBar.backgroundColor=UIColor(colorLiteralRed: 54/255, green: 175/255, blue: 188/255, alpha: 1)
        setNavigationBarHidden(false, animated:true)
    }
    
    public func setBG(){
        navigationBar.setBackgroundImage(UIImage(), for:UIBarMetrics.default)
        navigationBar.isTranslucent = true
        navigationBar.isOpaque=false
        navigationBar.shadowImage = UIImage()
        navigationBar.barTintColor=UIColor.clear
        setNavigationBarHidden(false, animated:true)
    }
}

extension UIView {
    
    func addBottomBorder(color:UIColor){
        let bottomBorder: CALayer = CALayer()
        bottomBorder.backgroundColor = color.cgColor
        bottomBorder.frame = CGRect(x: 0, y: self.frame.height-1, width: self.bounds.size.width, height: 1)
        self.layer.addSublayer(bottomBorder)
    }
    
    func addConstraintsWithFormat(_ format: String, views: UIView...) {
        var viewsDictionary = [String : UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
    func anchorToTop(_ top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil) {
        
        anchorWithConstantsToTop(top, left: left, bottom: bottom, right: right, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
    }
    
    func anchorWithConstantsToTop(_ top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil, topConstant: CGFloat = 0, leftConstant: CGFloat = 0, bottomConstant: CGFloat = 0, rightConstant: CGFloat = 0) {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: topConstant).isActive = true
        }
        
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -bottomConstant).isActive = true
        }
        
        if let left = left {
            leftAnchor.constraint(equalTo: left, constant: leftConstant).isActive = true
        }
        
        if let right = right {
            rightAnchor.constraint(equalTo: right, constant: -rightConstant).isActive = true
        }
    }
    
    func anchorWithConstantsToTop(top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil, topConstant: CGFloat = 0, leftConstant: CGFloat = 0, bottomConstant: CGFloat = 0, rightConstant: CGFloat = 0) {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: topConstant).isActive = true
        }
        
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -bottomConstant).isActive = true
        }
        
        if let left = left {
            leftAnchor.constraint(equalTo: left, constant: leftConstant).isActive = true
        }
        
        if let right = right {
            rightAnchor.constraint(equalTo: right, constant: -rightConstant).isActive = true
        }
    }
    
    func addConstraintsWithFormat(format: String, views: UIView...) {
        var viewsDictionary = [String : UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
    func anchorToTop(top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil) {
        
        anchorWithConstantsToTop(top: top, left: left, bottom: bottom, right: right, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
    }
}

extension Dictionary{
    func dict_to_string()->String{
        return (
            self
                .filter({
                    (key,value)->Bool in
                    return (key as! String) != "method"
                })
                .flatMap({
                    (key, value) -> String in
                    if (key as! String) == "url"{
                        return ""
                    }
                    if (key as! String) == "token"{
                        return ""
                    }
                    if (key as! String) == "method"{
                        return ""
                    }
                    return "\(key)=\(value)"
                }) as Array).joined(separator: "&")}
}
extension UserDefaults {
    
    enum UserDefaultsKeys: String {
        case info
        case favs
        case isLoggedIn
        case token
        case is_notified
        case is_agree
        case is_client
        case current_lang
        case type_1
        case type_2
    }
    
    func setLang(value:String){
        setValue(value, forKey: UserDefaultsKeys.current_lang.rawValue)
        UserDefaults.standard.synchronize()
    }
    func getLang()->String{
        if let lang=string(forKey: UserDefaultsKeys.current_lang.rawValue){
            return lang
        }
        return ""
    }
    
    func setToken(_ value: String) {
        setValue(value, forKey: UserDefaultsKeys.token.rawValue)
        UserDefaults.standard.synchronize()
    }
    func getToken() -> String {
        if let token=string(forKey: UserDefaultsKeys.token.rawValue){
            return token
        }
        return "c_8bJ46M-W0:APA91bFagzT8BKstd5wWxYJLjC5Kswny2Myt7H_uqYfyfIYTVvcH3SV20ExyL10Vpe4cfK_zzculy7YhTzmRygnFzqVuBWaeivXoyTkkAkgwok_8B9lFJ-fBr6sgEMuoTXDMMSZboGvd"
    }
    
    func setType1(_ value: Bool) {
        setValue(value, forKey: UserDefaultsKeys.type_1.rawValue)
        UserDefaults.standard.synchronize()
    }
    func isType1() -> Bool {
        return bool(forKey: UserDefaultsKeys.type_1.rawValue)
        //return false
    }
    
    func setType2(_ value: Bool) {
        setValue(value, forKey: UserDefaultsKeys.type_2.rawValue)
        UserDefaults.standard.synchronize()
    }
    func isType2() -> Bool {
        return bool(forKey: UserDefaultsKeys.type_2.rawValue)
        //return false
    }
    
    func setNotified(_ value: Bool) {
        setValue(value, forKey: UserDefaultsKeys.is_notified.rawValue)
        UserDefaults.standard.synchronize()
    }
    func isNotified() -> Bool {
        return bool(forKey: UserDefaultsKeys.is_notified.rawValue)
        //return false
    }
    
    func setAgried(_ value: Bool) {
        setValue(value, forKey: UserDefaultsKeys.is_agree.rawValue)
        UserDefaults.standard.synchronize()
    }
    func isAgried() -> Bool {
        return bool(forKey: UserDefaultsKeys.is_agree.rawValue)
        //return false
    }
    
    func setClient(_ value: Bool) {
        setValue(value, forKey: UserDefaultsKeys.is_client.rawValue)
        UserDefaults.standard.synchronize()
    }
    func isClient() -> Bool {
        if let value=string(forKey: UserDefaultsKeys.is_client.rawValue){
            return bool(forKey: value)
        }
        //        return true
        return false
    }
    
    //Detect if user authorized
    func setIsLoggedIn(_ value: Bool) {
        setValue(value, forKey: UserDefaultsKeys.isLoggedIn.rawValue)
        UserDefaults.standard.synchronize()
    }
    func isLoggedIn() -> Bool {
        return bool(forKey: UserDefaultsKeys.isLoggedIn.rawValue)
        //return false
    }
    //info
    func setInfo(_ value:[String:String]){
        setValue(value, forKey: UserDefaultsKeys.info.rawValue)
        UserDefaults.standard.synchronize()
    }
    func getInfo()->[String:String]{
        if let f=dictionary(forKey: UserDefaultsKeys.info.rawValue){
            return f as! [String : String]
        }
        return [:]
    }
    //Favs
    func setNewFav(_ value:String,key:String){
        if var f=dictionary(forKey: UserDefaultsKeys.favs.rawValue){
            f[key]=value
            setValue(f, forKey: UserDefaultsKeys.favs.rawValue)
        }else{
            var f:[String:String]=[String:String]()
            f[key]=value
            setValue(f, forKey: UserDefaultsKeys.favs.rawValue)
        }
        UserDefaults.standard.synchronize()
    }
    
    func getFavs()->[String:String]{
        if let f=dictionary(forKey: UserDefaultsKeys.favs.rawValue){
            return f as! [String : String]
        }
        return [:]
    }
    
    func deletFavs(key:String){
        var f=getFavs()
        if f.isEmpty==false{
            if f[key] != nil{
                f.removeValue(forKey:key)
                setValue(f, forKey: UserDefaultsKeys.favs.rawValue)
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    func isFaved(key:String)->Bool{
        let f=getFavs()
        if f.isEmpty==false{
            if f[key] != nil{
                return true
            }
        }
        return false
    }
}
