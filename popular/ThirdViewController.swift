//
//  ThirdViewController.swift
//  popular
//
//  Created by beka on 3/28/17.
//  Copyright © 2017 beka. All rights reserved.
//

import UIKit
import SwiftyJSON

class ThirdViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var tag = -1
    var message=JSON.null
    var params=[String:String]()
    var loadingView:UIView={
        let l=UIView()
        l.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive=true
        l.backgroundColor=UIColor.black.withAlphaComponent(0.4)
        l.translatesAutoresizingMaskIntoConstraints=false
        let activity=UIActivityIndicatorView()
        activity.startAnimating()
        activity.activityIndicatorViewStyle = .whiteLarge
        activity.translatesAutoresizingMaskIntoConstraints=false
        l.addSubview(activity)
        activity.centerXAnchor.constraint(equalTo: l.centerXAnchor).isActive=true
        activity.centerYAnchor.constraint(equalTo: l.centerYAnchor).isActive=true
        l.isHidden=true
        return l
    }()
    var loadingViewHeght:NSLayoutConstraint?
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var add: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(loadingView)
        loadingViewHeght=NSLayoutConstraint(item: loadingView, attribute:NSLayoutAttribute.height, relatedBy:NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: UIScreen.main.bounds.height)
        loadingView.addConstraint(loadingViewHeght!)
        loadingView.anchorWithConstantsToTop(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        switch tag {
        case 0:
            if UserDefaults.standard.isClient(){
                navigationItem.rightBarButtonItems=[]
            }
            self.title="animal".localize(lang: lang)
             params["method"]="get"
            params["url"]="/api/List/Vid_pererabotki?vid=true"
            load(params: params)
            break
        case 1:
            if UserDefaults.standard.isClient(){
                navigationItem.rightBarButtonItems=[]
            }
            self.title="flower".localize(lang: lang)
             params["method"]="get"
            params["url"]="/api/List/Vid_pererabotki?vid=false"
            load(params: params)
            break
        default:
            navigationItem.rightBarButtonItems=[]
            self.title="type_fin".localize(lang: lang)
             params["method"]="get"
            params["url"]="/api/List/Vid_uslug"
            load(params: params)
            break
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func load(params: [String : String]){
        loadingView.isHidden=false
        CustomRequests.request(params: params){
            response in
            self.loadingView.isHidden=true
            if response.result.error == nil{
                let result=JSON(response.result.value!)
                if result != JSON.null{
                    self.message=result
                    self.table.reloadData()
                }
            }else{
                let cancel=UIAlertAction(title: "Закрыть", style: .destructive){
                    (result:UIAlertAction)->Void in
                    
                }
                self.showAlert(msg: "Произошла ошибка. Повторите попытку", actions: [cancel])
            }
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return message.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text=message[indexPath.row]["name"].stringValue
        switch tag {
        case 0:
            cell.imageView?.image=#imageLiteral(resourceName: "yellow")
            break
        case 1:
            cell.imageView?.image=#imageLiteral(resourceName: "green")
            break
        default:
            cell.imageView?.image=#imageLiteral(resourceName: "red")
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        var id=""
        switch tag {
        case 0,1:
            id=message[indexPath.row]["Id"].stringValue
            break
        case 2:
            id=message[indexPath.row]["id"].stringValue
            break
        default:
            break
        }
        performSegue(withIdentifier: "show", sender: id)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        if segue.identifier=="show"{
            let dc=segue.destination as! LastTableViewController
            dc.id=sender as! String
            dc.tag=tag
        }
    }
}
