//
//  LastTableViewController.swift
//  popular
//
//  Created by beka on 3/28/17.
//  Copyright © 2017 beka. All rights reserved.
//

import UIKit
import SwiftyJSON

class LastTableViewController: UITableViewController,UISearchBarDelegate{
    
    var tag = -1
    var id=""
    var message=JSON.null
    var params=[String:String]()
    var loadingView:UIView={
        let l=UIView()
        l.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive=true
        l.backgroundColor=UIColor.black.withAlphaComponent(0.4)
        l.translatesAutoresizingMaskIntoConstraints=false
        let activity=UIActivityIndicatorView()
        activity.startAnimating()
        activity.activityIndicatorViewStyle = .whiteLarge
        activity.translatesAutoresizingMaskIntoConstraints=false
        l.addSubview(activity)
        activity.centerXAnchor.constraint(equalTo: l.centerXAnchor).isActive=true
        activity.centerYAnchor.constraint(equalTo: l.centerYAnchor).isActive=true
        l.isHidden=true
        return l
    }()
    var loadingViewHeght:NSLayoutConstraint?
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var add: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(loadingView)
        loadingViewHeght=NSLayoutConstraint(item: loadingView, attribute:NSLayoutAttribute.height, relatedBy:NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: UIScreen.main.bounds.height)
        loadingView.addConstraint(loadingViewHeght!)
        loadingView.anchorWithConstantsToTop(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        switch tag {
        case -1:
            navigationItem.rightBarButtonItems=[]
            self.title="my".localize(lang: lang)
             params["method"]="get"
            params["url"]="/api/List/My"
            load(params: params)
            break
        case 0,1:
            if UserDefaults.standard.isClient(){
                navigationItem.rightBarButtonItems=[]
            }
            self.title="organization".localize(lang: lang)
             params["method"]="get"
            params["url"]="/api/List/PrepriatieSpisok?pererabotkaid=\(id)&oblastid=&orderby=&page=1&pagesize=10&name="
            load(params: params)
            break
        case 3:
            navigationItem.rightBarButtonItems=[]
            tableView.tableHeaderView=UIView()
             params["method"]="get"
            params["url"]="/api/List/Subsidia?page=1&pagesize=10&name="
            load(params: params)
            break
        default:
            navigationItem.rightBarButtonItems=[]
            self.title="organization".localize(lang: lang)
             params["method"]="get"
            params["url"]="/api/List/Finansovie_organizacii?vidi_uslugid=\(id)&oblastid=&orderby=&page=1&pagesize=10&name="
            load(params: params)
            break
        }
        
        self.tableView.estimatedRowHeight=50
        self.tableView.rowHeight=UITableViewAutomaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func load(params: [String : String]){
        loadingView.isHidden=false
        CustomRequests.request(params: params){
            response in
            self.loadingView.isHidden=true
            if response.result.error == nil{
                let result=JSON(response.result.value!)
                if result != JSON.null{
                    self.message=result
                    self.tableView.reloadData()
                }
            }else{
                let cancel=UIAlertAction(title: "Закрыть", style: .destructive){
                    (result:UIAlertAction)->Void in
                    
                }
                self.showAlert(msg: "Произошла ошибка. Повторите попытку", actions: [cancel])
            }
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        if searchBar.text == ""{
            switch tag {
            case -1:
                break
            case 0,1:
                 params["method"]="get"
                params["url"]="/api/List/PrepriatieSpisok?pererabotkaid=\(id)&oblastid=&orderby=&page=1&pagesize=10&name="
                load(params: params)
                break
            case 3:
                break
            default:
                 params["method"]="get"
                params["url"]="/api/List/Finansovie_organizacii?vidi_uslugid=\(id)&oblastid=&orderby=&page=1&pagesize=10&name="
                load(params: params)
                break
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        if let text=searchBar.text{
            switch tag {
            case -1:
                break
            case 0,1:
                 params["method"]="get"
                params["url"]="/api/List/PrepriatieSpisok?pererabotkaid=\(id)&oblastid=&orderby=&page=1&pagesize=10&name=\(text)"
                load(params: params)
                break
            case 3:
                break
            default:
                 params["method"]="get"
                params["url"]="/api/List/Finansovie_organizacii?vidi_uslugid=\(id)&oblastid=&orderby=&page=1&pagesize=10&name=\(text)"
                load(params: params)
                break
            }
        }
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return message.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tag==3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! SubsidiaCell
            
            cell.title.text=message[indexPath.row]["title"].stringValue
            
            if let d=message[indexPath.row]["date"].string{
                cell.data.text=d
            }
            
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text=message[indexPath.row]["naimenovanie"].stringValue
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if tag==3{
            //performSegue(withIdentifier: "show", sender: message[indexPath.row]["id"].stringValue)
        }else{
            performSegue(withIdentifier: "show", sender: message[indexPath.row]["Id"].stringValue)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        if segue.identifier=="show"{
            let dc=segue.destination as! DetailTableViewController
            dc.tag=tag
            dc.id=sender as! String
        }
    }
}
class SubsidiaCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var desk: UILabel!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var data: UILabel!
}
