//
//  LoginViewController.swift
//  popular
//
//  Created by beka on 3/20/17.
//  Copyright © 2017 beka. All rights reserved.
//

import UIKit
import SwiftyJSON

class LoginViewController: UIViewController,AuthhDelegate {
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var header: UIView!
    var params=[String:String]()
    var loadingView:UIView={
        let l=UIView()
        l.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive=true
        l.backgroundColor=UIColor.black.withAlphaComponent(0.4)
        l.translatesAutoresizingMaskIntoConstraints=false
        let activity=UIActivityIndicatorView()
        activity.startAnimating()
        activity.activityIndicatorViewStyle = .whiteLarge
        activity.translatesAutoresizingMaskIntoConstraints=false
        l.addSubview(activity)
        activity.centerXAnchor.constraint(equalTo: l.centerXAnchor).isActive=true
        activity.centerYAnchor.constraint(equalTo: l.centerYAnchor).isActive=true
        l.isHidden=true
        return l
    }()
    var loadingViewHeght:NSLayoutConstraint?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title="login".localize(lang: lang)
        self.hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        view.addSubview(loadingView)
        loadingViewHeght=NSLayoutConstraint(item: loadingView, attribute:NSLayoutAttribute.height, relatedBy:NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: UIScreen.main.bounds.height)
        loadingView.addConstraint(loadingViewHeght!)
        loadingView.anchorWithConstantsToTop(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func load(params: [String : String]){
        loadingView.isHidden=false
        CustomRequests.request(params: params){
            response in
            self.loadingView.isHidden=true
            if response.result.error == nil{
                let result=JSON(response.result.value!)
                if result["access_token"] != ""{
                    print(result)
                    UserDefaults.standard.setIsLoggedIn(true)
                    UserDefaults.standard.setToken(result["access_token"].stringValue)
                    self.performSegue(withIdentifier: "login", sender: nil)
                }else{
                    let cancel=UIAlertAction(title: "Закрыть", style: .destructive){
                        (result:UIAlertAction)->Void in
                        
                    }
                    self.showAlert(msg: "Произошла ошибка. Повторите попытку", actions: [cancel])
                }
            }else{
                let cancel=UIAlertAction(title: "Закрыть", style: .destructive){
                    (result:UIAlertAction)->Void in
                    
                }
                self.showAlert(msg: "Произошла ошибка. Повторите попытку", actions: [cancel])
            }
        }
    }
    
    internal func request(params: [String : String]) {
        load(params: params)
    }
    
    internal func show() {
        performSegue(withIdentifier: "login", sender: nil)
    }
    
    internal func scroll(point: CGPoint) {
        self.table.contentOffset.y=point.y+header.bounds.height/2
    }
    
    internal func dismiss() {
        self.view.endEditing(true)
        self.table.contentOffset.y=CGPoint.zero.y
    }
    
    func keyboardWillHide(notification: Notification){
        dismiss()
    }
    
    func keyboardWillShow(notification: Notification){
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        if segue.identifier=="login"{
            
        }else if segue.identifier=="forget"{
            let dc=segue.destination as! ForgetViewController
            dc.t="forget"
        }
    }
}
extension LoginViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LoginCell
        cell.delegate=self
        return cell
    }
}
class LoginCell: UITableViewCell,UITextFieldDelegate{
    
    @IBOutlet weak var login: UITextField!
    @IBOutlet weak var pass: UITextField!
    @IBOutlet weak var login_btn: UIButton!
    
    let toolBar:UIToolbar={
        let t=UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 30))
        return t
    }()
    var delegate:AuthhDelegate?
    var params=[String:String]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        toolBar.items=[
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Закрыть", style: .plain, target: self, action: #selector(dismissKeyboard))
        ]
        login_btn.layer.borderWidth=1
        login_btn.layer.borderColor=UIColor.white.cgColor
        (login as! VSTextField).setFormatting("#(###)###-##-##", replacementChar: "#")
        
        login.layer.borderWidth=1
        login.layer.borderColor=UIColor.gray.cgColor
        login.inputAccessoryView=toolBar
        
        pass.layer.borderWidth=1
        pass.layer.borderColor=UIColor.gray.cgColor
        pass.inputAccessoryView=toolBar
    }
    
    func dismissKeyboard() {
        delegate?.dismiss()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print(textField.frame.origin)
        delegate?.scroll(point: textField.frame.origin)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField==login{
            pass.becomeFirstResponder()
        }
        return true
    }
    
    @IBAction func login(_ sender: Any) {
        params["method"]="post"
        params["url"]="/Token"
        params["grant_type"]="password"
        if let text=login.text{
            params["username"]=text
        }
        if let text=pass.text{
            params["Password"]=text
        }
        delegate?.request!(params: params)
    }
    
    @IBAction func forget(_ sender: Any) {
        
    }
}
