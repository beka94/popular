//
//  SettingsViewController.swift
//  popular
//
//  Created by beka on 3/17/17.
//  Copyright © 2017 beka. All rights reserved.
//

import UIKit
import MMDrawerController

class SettingsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var agree: UISwitch!
    var loadingView:UIView={
        let l=UIView()
        l.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive=true
        l.backgroundColor=UIColor.black.withAlphaComponent(0.4)
        l.translatesAutoresizingMaskIntoConstraints=false
        l.isHidden=true
        return l
    }()
    var loadingViewHeght:NSLayoutConstraint?
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title="setting".localize(lang: lang)
        
        view.addSubview(loadingView)
        loadingViewHeght=NSLayoutConstraint(item: loadingView, attribute:NSLayoutAttribute.height, relatedBy:NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: UIScreen.main.bounds.height)
        loadingView.addConstraint(loadingViewHeght!)
        loadingView.anchorWithConstantsToTop(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        self.mm_drawerController.setGestureCompletionBlock{
            (drawerController,gesture) -> Void in
            self.loadingView.isHidden=true
        }

        table.tableFooterView=UIView()
        agree.setOn(UserDefaults.standard.isAgried(), animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func open(_ sender: Any) {
        self.mm_drawerController.open(MMDrawerSide.left, animated: true, completion: {
            _ in
            self.loadingView.isHidden=false
        })
    }
    
    @IBAction func change(_ sender: UISwitch) {
        UserDefaults.standard.setAgried(sender.isOn)
        agree.setOn(UserDefaults.standard.isAgried(), animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text="lang".localize(lang: lang)
        cell.detailTextLabel?.text=lang=="kz" ? "Қазақша" : "Русский"
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let alertController = UIAlertController(title: "lang".localize(lang: lang), message: "", preferredStyle: .actionSheet)
        
        let btn1 = UIAlertAction(title: "Қазақ", style: .default, handler:  { void in
            UserDefaults.standard.setLang(value: "kz")
            lang=UserDefaults.standard.getLang()
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "setting")
            self.mm_drawerController.setCenterView(centerViewController, withCloseAnimation: true, completion: nil)
            menu_delegate?.reload()
        })
        let btn2 = UIAlertAction(title: "Русский", style: .default, handler:  { void in
            UserDefaults.standard.setLang(value: "rus")
            lang=UserDefaults.standard.getLang()
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "setting")
            self.mm_drawerController.setCenterView(centerViewController, withCloseAnimation: true, completion: nil)
            menu_delegate?.reload()
        })
        let cancelAction = UIAlertAction(title: "close".localize(lang: lang), style: .cancel, handler:  { void in
            
        })
        alertController.addAction(btn1)
        alertController.addAction(btn2)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}
