//
//  MenuViewController.swift
//  popular
//
//  Created by beka on 3/15/17.
//  Copyright © 2017 beka. All rights reserved.
//

import UIKit
import MMDrawerController

var lang=UserDefaults.standard.getLang()
var menu_delegate:MenuDelegate?
protocol MenuDelegate {
    func reload()
}
class MenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,MenuDelegate {
    
    internal func reload() {
        table.reloadData()
    }

    @IBOutlet weak var table: UITableView!
    let menu_items=["main","profile","setting","about"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.mm_drawerController.openDrawerGestureModeMask = .all
        self.mm_drawerController.closeDrawerGestureModeMask = .all
        self.mm_drawerController.setMaximumLeftDrawerWidth(UIScreen.main.bounds.width*2/3, animated: false, completion: nil)
//        self.mm_drawerController.setDrawerVisualStateBlock{ (drawerController, drawerSide, percentVisible) -> Void in
//            let sideDrawerViewController=drawerController?.centerViewController
//            sideDrawerViewController?.view?.alpha = percentVisible
//        }
        
//        self.mm_drawerController.setGestureCompletionBlock{
//            (drawerController,gesture) -> Void in
//            if drawerController?.visibleLeftDrawerWidth != 0{
//                let sideDrawerViewController=drawerController?.centerViewController
//                sideDrawerViewController?.view?.alpha = 6
//            }
//        }
        

        menu_delegate=self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu_items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text=menu_items[indexPath.row].localize(lang: lang)
       
        if indexPath.row==0{
            cell.imageView?.image=#imageLiteral(resourceName: "brand")
        }else{
            cell.imageView?.image=UIImage(named: menu_items[indexPath.row])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        switch indexPath.row {
        case 0:
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "home")
            self.mm_drawerController.setCenterView(centerViewController, withCloseAnimation: true, completion: nil)
            break
        case 1:
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "profile")
            self.mm_drawerController.setCenterView(centerViewController, withCloseAnimation: true, completion: nil)
            break
        case 2:
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "setting")
            self.mm_drawerController.setCenterView(centerViewController, withCloseAnimation: true, completion: nil)
            break
        default:
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "about")
            self.mm_drawerController.setCenterView(centerViewController, withCloseAnimation: true, completion: nil)
            break
        }
    }
}
class MenuCell: UITableViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var title: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.img.frame=CGRect(x: 30, y: 5, width: 30, height: 30)
        self.textLabel?.frame.origin=CGPoint(x: 80, y: 0)
    }
}

