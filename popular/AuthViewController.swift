//
//  AuthViewController.swift
//  popular
//
//  Created by beka on 3/15/17.
//  Copyright © 2017 beka. All rights reserved.
//

import UIKit
import SwiftyJSON

@objc protocol AuthhDelegate {
    func scroll(point:CGPoint)
    func dismiss()
    func show()
    @objc optional func request(params:[String:String])
}
class AuthViewController: UIViewController,AuthhDelegate {
    
    @IBOutlet weak var table: UITableView!
    var params=[String:String]()
    var loadingView:UIView={
        let l=UIView()
        l.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive=true
        l.backgroundColor=UIColor.black.withAlphaComponent(0.4)
        l.translatesAutoresizingMaskIntoConstraints=false
        let activity=UIActivityIndicatorView()
        activity.startAnimating()
        activity.activityIndicatorViewStyle = .whiteLarge
        activity.translatesAutoresizingMaskIntoConstraints=false
        l.addSubview(activity)
        activity.centerXAnchor.constraint(equalTo: l.centerXAnchor).isActive=true
        activity.centerYAnchor.constraint(equalTo: l.centerYAnchor).isActive=true
        l.isHidden=true
        return l
    }()
    var loadingViewHeght:NSLayoutConstraint?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title="register".localize(lang: lang)
        self.hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        view.addSubview(loadingView)
        loadingViewHeght=NSLayoutConstraint(item: loadingView, attribute:NSLayoutAttribute.height, relatedBy:NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: UIScreen.main.bounds.height)
        loadingView.addConstraint(loadingViewHeght!)
        loadingView.anchorWithConstantsToTop(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        table.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func load(params: [String : String]){
        loadingView.isHidden=false
        CustomRequests.request(params: params){
            response in
            self.loadingView.isHidden=true
            if response.result.error == nil{
                let result=JSON(response.result.value!)
                if result["responce"].stringValue == "Your registration was successfull"{
                    let p=[
                        "method":"post",
                        "url":"/Token",
                        "grant_type":"password",
                        "username":params["PhoneNumber"],
                        "Password":params["Password"]
                    ]
                    self.loadingView.isHidden=false
                    CustomRequests.request(params: p as! [String : String]){
                        response in
                        self.loadingView.isHidden=true
                        if response.result.error == nil{
                            let result=JSON(response.result.value!)
                            if result["access_token"] != ""{
                                print(result)
                                UserDefaults.standard.setIsLoggedIn(true)
                                UserDefaults.standard.setToken(result["access_token"].stringValue)
                                self.performSegue(withIdentifier: "show", sender: nil)
                            }else{
                                let cancel=UIAlertAction(title: "Закрыть", style: .destructive){
                                    (result:UIAlertAction)->Void in
                                    
                                }
                                self.showAlert(msg: "Произошла ошибка. Повторите попытку", actions: [cancel])
                            }
                        }else{
                            let cancel=UIAlertAction(title: "Закрыть", style: .destructive){
                                (result:UIAlertAction)->Void in
                                
                            }
                            self.showAlert(msg: "Произошла ошибка. Повторите попытку", actions: [cancel])
                        }
                    }
                }else{
                    let cancel=UIAlertAction(title: "Закрыть", style: .destructive){
                        (result:UIAlertAction)->Void in
                        
                    }
                    self.showAlert(msg: "Произошла ошибка. Повторите попытку", actions: [cancel])
                }
            }else{
                let cancel=UIAlertAction(title: "Закрыть", style: .destructive){
                    (result:UIAlertAction)->Void in
                    
                }
                self.showAlert(msg: "Произошла ошибка. Повторите попытку", actions: [cancel])
            }
        }
    }
    
    internal func request(params: [String : String]) {
        load(params: params)
    }
    
    internal func show() {
        performSegue(withIdentifier: "oblast", sender: nil)
    }
    
    internal func scroll(point: CGPoint) {
        self.table.contentOffset.y=point.y
    }
    
    internal func dismiss() {
        self.view.endEditing(true)
        self.table.contentOffset.y=CGPoint.zero.y
    }
    
    func keyboardWillHide(notification: Notification){
        dismiss()
    }
    
    func keyboardWillShow(notification: Notification){
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier=="show"{
            UserDefaults.standard.setIsLoggedIn(true)
        }
    }
}
extension AuthViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AuthCell
        cell.delegate=self
        cell.parent=self
        if oblastid != ""{
            cell.fields[0].text="\(selected_oblast),\(selected_city),\(selected_selo)"
        }
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
    }
}
var oblastid=""
var gorodid=""
var seloid=""
var selected_oblast="Область"
var selected_city="Город"
var selected_selo="Село"
class AuthCell: UITableViewCell {
    @IBOutlet var fields: [UITextField]!
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var sw_1: UISwitch!
    @IBOutlet weak var sw_1_h: NSLayoutConstraint!

    let toolBar:UIToolbar={
        let t=UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 30))
        return t
    }()
    var delegate:AuthhDelegate?
    var parent:UIViewController?
    var params=[String:String]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        toolBar.items=[
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Закрыть", style: .plain, target: self, action: #selector(dismissKeyboard))
        ]
 
        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
        let underlineAttributedString = NSAttributedString(string: "Принимаю соглашение", attributes: underlineAttribute)
        lbl.attributedText = underlineAttributedString
        let gesture=UITapGestureRecognizer(target: self, action: #selector(show_agree))
        lbl.addGestureRecognizer(gesture)

        btn.setTitle("register".localize(lang: lang), for: .normal)
        
        for i in 0...fields.count-1 {
            let item=fields[i]
            item.inputAccessoryView=toolBar
            item.layer.borderWidth=1
            item.layer.borderColor=UIColor.gray.cgColor
            if i==3 || i==6{
              (item as! VSTextField).setFormatting("#(###)###-##-##", replacementChar: "#")
            }
        }
        btn.layer.borderWidth=1
        btn.layer.borderColor=UIColor.white.cgColor
        
        sw_1_h.constant=0
        
    }
    
    func dismissKeyboard() {
        delegate?.dismiss()
    }

    @IBAction func register(_ sender: Any) {
        params["method"]="post"
        params["url"]="/api/Account/Register"
        params["oblastid"]=oblastid
        params["gorodid"]=gorodid
        params["seloid"]=seloid
        if UserDefaults.standard.isClient(){
            params["roleid"]="17673cea-08c8-44a0-a8b1-35dc30584927"
        }else{
            params["roleid"]="2aeceb63-0894-4479-9bcd-3ee424926f72"
        }
        for i in 0...fields.count-1 {
            if i==6{
                if sw_1.isOn == false{
                    params["priglasitel"]=""
                    continue
                }
            }
            let item=fields[i]
            switch i {
            case 1:
                if let text=item.text{
                    params["naimenovanie"]=text
                }
                break
            case 2:
                if let text=item.text{
                    params["FIO"]=text
                }
                break
            case 3:
                if let text=item.text{
                    params["PhoneNumber"]=text
                }
                break
            case 4:
                if let text=item.text{
                    params["Email"]=text
                }
                break
            case 5:
                if let text=item.text{
                    params["Password"]=text
                    params["ConfirmPassword"]=text
                }
                break
            case 6:
                if let text=item.text{
                    params["priglasitel"]=text
                }
            case 6:
                if let text=item.text{
                    params["Adress"]=text
                }
                break
            default:
                break
            }
        }
        delegate?.request!(params: params)
    }
    
    @IBAction func change(_ sender: UISwitch) {
        if sender==sw_1{
            if sender.isOn{
                sw_1_h.constant=50
            }else{
                sw_1_h.constant=0
            }
        }else{
            if sender.isOn{
                show_agree(sender: sender)
            }else{
                UserDefaults.standard.setAgried(false)
            }
        }
    }
    
    func show_agree(sender: UISwitch){
        let alertController = UIAlertController(title: "", message: "Подвердите согласию на платную подписку от Popular-plus.kz отправив код: 2525 на номер 5050!", preferredStyle: .actionSheet)
        
        let btn1 = UIAlertAction(title: "Да", style: .default, handler:  { void in
            UserDefaults.standard.setAgried(true)
        })
        let cancelAction = UIAlertAction(title: "close".localize(lang: lang), style: .cancel, handler:  { void in
            sender.isOn=false
        })
        alertController.addAction(btn1)
        alertController.addAction(cancelAction)
        
        parent?.present(alertController, animated: true, completion: nil)
    }
}
extension AuthCell:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField==fields[0]{
            textField.resignFirstResponder()
            delegate?.show()
        }else{
            delegate?.scroll(point: textField.frame.origin)
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        delegate?.dismiss()
        return true
    }
}
