//
//  ProfileViewController.swift
//  popular
//
//  Created by beka on 3/23/17.
//  Copyright © 2017 beka. All rights reserved.
//

import UIKit
import MMDrawerController

protocol ProfileDelegate {
    func scroll(point:CGPoint)
    func dismiss()
    func reload()
    func set(val:Bool)
}

class ProfileViewController: UIViewController,ProfileDelegate {
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var footer: UIView!
    @IBOutlet weak var logout: UIButton!
    @IBOutlet weak var logout_top: NSLayoutConstraint!
    @IBOutlet weak var lbl: UILabel!
    var loadingView:UIView={
        let l=UIView()
        l.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive=true
        l.backgroundColor=UIColor.black.withAlphaComponent(0.4)
        l.translatesAutoresizingMaskIntoConstraints=false
        l.isHidden=true
        return l
    }()
    var loadingViewHeght:NSLayoutConstraint?
    var isEdite=false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title="profile".localize(lang: lang)
        
        view.addSubview(loadingView)
        loadingViewHeght=NSLayoutConstraint(item: loadingView, attribute:NSLayoutAttribute.height, relatedBy:NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: UIScreen.main.bounds.height)
        loadingView.addConstraint(loadingViewHeght!)
        loadingView.anchorWithConstantsToTop(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        self.mm_drawerController.setGestureCompletionBlock{
            (drawerController,gesture) -> Void in
            self.loadingView.isHidden=true
        }
        
        table.estimatedRowHeight=100
        table.rowHeight=UITableViewAutomaticDimension
        
        if UserDefaults.standard.isAgried()==false{
            lbl.isHidden=true
            logout_top.constant = -30
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func open(_ sender: Any) {
        self.mm_drawerController.open(MMDrawerSide.left, animated: true, completion: {
            _ in
            self.loadingView.isHidden=false
        })
    }
    
    @IBAction func logout_act(_ sender: Any) {
        UserDefaults.standard.setIsLoggedIn(false)
        UserDefaults.standard.setType1(false)
        UserDefaults.standard.setType2(false)
        UserDefaults.standard.setNotified(false)
        UserDefaults.standard.setAgried(false)
        UserDefaults.standard.setClient(false)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        appDelegate.window?.rootViewController=mainStoryBoard.instantiateViewController(withIdentifier: "first") as! FirstViewController
    }
    
    internal func reload() {
        table.reloadData()
    }
    
    internal func set(val:Bool) {
        isEdite=val
    }
    
    internal func scroll(point: CGPoint) {
        self.table.contentOffset.y=point.y
    }
    
    internal func dismiss() {
        self.view.endEditing(true)
        self.table.contentOffset.y=CGPoint.zero.y+100
    }
    
    func keyboardWillHide(notification: Notification){
       
    }
    
    func keyboardWillShow(notification: Notification){
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        if segue.identifier=="list"{
            let dc=segue.destination as! LastTableViewController
            dc.tag = -1
        }else  if segue.identifier=="forget"{
            let dc=segue.destination as! ForgetViewController
            dc.t="change"
        }
    }
}
extension ProfileViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if UserDefaults.standard.isAgried()==false{
            return 3
        }
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row==0{
            let cell=tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProfileCell
            cell.is_first=true
            cell.delegate=self
            cell.parent=self
            return cell
        }
        if indexPath.row==2{
            let cell=tableView.dequeueReusableCell(withIdentifier: "agree", for: indexPath) as! ProfileCell
            cell.delegate=self
            cell.parent=self
            return cell
        }
        if UserDefaults.standard.isClient(){
            let cell=tableView.dequeueReusableCell(withIdentifier:"client", for: indexPath) as! ProfileCell
            cell.is_client=true
            cell.delegate=self
            cell.parent=self
            return cell
        }
        let cell=tableView.dequeueReusableCell(withIdentifier:"not_client", for: indexPath) as! ProfileCell
        cell.is_not_client=true
        cell.delegate=self
        cell.parent=self
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row != 0{
            return UITableViewAutomaticDimension
        }
        if isEdite{
            return 400
        }
        return 350
    }
}
extension ProfileCell:UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        save_btn.isHidden=false
        top.constant=60
        delegate?.set(val: true)
        delegate?.reload()
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.scroll(point: textField.frame.origin)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        delegate?.dismiss()
        return true
    }
}
class ProfileCell: UITableViewCell {
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var tel: UITextField!
    @IBOutlet weak var hoz: UITextField!
    @IBOutlet weak var mail: UITextField!
    @IBOutlet weak var type_1: UISwitch!
    @IBOutlet weak var type1_lbl: UILabel!
    @IBOutlet weak var type_2: UISwitch!
    @IBOutlet weak var type2_lbl: UILabel!
    @IBOutlet weak var add_btn: UIButton!
    @IBOutlet weak var save_btn: UIButton!
    @IBOutlet weak var top: NSLayoutConstraint!
    var isEdite=false
    
    var label:UILabel=UILabel()
    let toolBar:UIToolbar={
        let t=UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 30))
        return t
    }()
    var delegate:ProfileDelegate?
    var parent:UIViewController?
    
    override func awakeFromNib() {
        toolBar.items=[
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Закрыть", style: .plain, target: self, action: #selector(dismissKeyboard))
        ]
    }
    
    @IBAction func save(_ sender: UIButton) {
        save_btn.isHidden=true
        top.constant=10
        delegate?.set(val: false)
        delegate?.reload()
    }
    
    func dismissKeyboard() {
        delegate?.dismiss()
    }
    
    var is_first:Bool?{
        didSet{
            if is_first!{
                top.constant=top != nil ? top.constant:10
                save_btn.isHidden=save_btn.isHidden
                name.inputAccessoryView=toolBar
                tel.inputAccessoryView=toolBar
                hoz.inputAccessoryView=toolBar
                mail.inputAccessoryView=toolBar
                
                label=make_lbl(text: "ФИО")
                name.addSubview(label)
                label.anchorWithConstantsToTop(name.topAnchor, left: name.leftAnchor, bottom: nil, right: name.rightAnchor, topConstant: -2, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
                name.addBottomBorder(color: UIColor.lightGray)
                
                label=make_lbl(text: "Телефон")
                tel.addSubview(label)
                label.anchorWithConstantsToTop(tel.topAnchor, left: tel.leftAnchor, bottom: nil, right: tel.rightAnchor, topConstant: -2, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
                tel.addBottomBorder(color: UIColor.lightGray)
                
                label=make_lbl(text: "Наименования хозяйства")
                hoz.addSubview(label)
                label.anchorWithConstantsToTop(hoz.topAnchor, left: hoz.leftAnchor, bottom: nil, right: hoz.rightAnchor, topConstant: -2, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
                hoz.addBottomBorder(color: UIColor.lightGray)
                
                label=make_lbl(text: "Электронная почта")
                mail.addSubview(label)
                label.anchorWithConstantsToTop(mail.topAnchor, left: mail.leftAnchor, bottom: nil, right: mail.rightAnchor, topConstant: -2, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
                mail.addBottomBorder(color: UIColor.lightGray)
            }
        }
    }
    
    var is_client:Bool?{
        didSet{
            type1_lbl.text="animal".localize(lang: lang)
            type2_lbl.text="flower".localize(lang: lang)
            if is_client!{
                type_1.addTarget(self, action: #selector(change(sender:)), for: .valueChanged)
                type_2.addTarget(self, action: #selector(change(sender:)), for: .valueChanged)
                
                type_1.setOn(UserDefaults.standard.isType1(), animated: false)
                type_2.setOn(UserDefaults.standard.isType2(), animated: false)
            }
        }
    }
    
    var is_not_client:Bool?{
        didSet{
            if is_not_client!{
                add_btn.layer.borderWidth=1
                add_btn.layer.borderColor=UIColor(colorLiteralRed: 54/255, green: 175/255, blue: 188/255, alpha: 1).cgColor
            }
        }
    }
    
    var is_agree:Bool?{
        didSet{
            if is_agree!{
                
            }
        }
    }
    
    
    @IBAction func agree(_ sender: UISwitch) {
        if sender.isOn{
            show_agree(sender: sender)
        }else{
            UserDefaults.standard.setAgried(false)
        }
    }
    
    func show_agree(sender: UISwitch){
        let alertController = UIAlertController(title: "", message: "Подвердите согласию на платную подписку от Popular-plus.kz отправив код: 2525 на номер 5050!", preferredStyle: .actionSheet)
        
        let btn1 = UIAlertAction(title: "Да", style: .default, handler:  { void in
            UserDefaults.standard.setAgried(true)
        })
        let cancelAction = UIAlertAction(title: "close".localize(lang: lang), style: .cancel, handler:  { void in
            sender.isOn=false
        })
        alertController.addAction(btn1)
        alertController.addAction(cancelAction)
        
        parent?.present(alertController, animated: true, completion: nil)
    }
    
    func make_lbl(text:String)->UILabel{
        let label=UILabel()
        label.textColor=UIColor.gray
        label.text=text
        label.font=UIFont.systemFont(ofSize: 10)
        return label
    }
    
    func change(sender:UISwitch){
        if sender==type_1{
            UserDefaults.standard.setType1(type_1.isOn)
        }else{
            UserDefaults.standard.setType2(type_2.isOn)
        }
    }
}
