//
//  HomeViewController.swift
//  popular
//
//  Created by beka on 3/16/17.
//  Copyright © 2017 beka. All rights reserved.
//

import UIKit
import MMDrawerController

class HomeViewController: UIViewController {
    
    
    @IBOutlet var views: [UIView]!
    var loadingView:UIView={
        let l=UIView()
        l.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive=true
        l.backgroundColor=UIColor.black.withAlphaComponent(0.4)
        l.translatesAutoresizingMaskIntoConstraints=false
        l.isHidden=true
        return l
    }()
    var loadingViewHeght:NSLayoutConstraint?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title="main".localize(lang: lang)
        
        view.addSubview(loadingView)
        loadingViewHeght=NSLayoutConstraint(item: loadingView, attribute:NSLayoutAttribute.height, relatedBy:NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: UIScreen.main.bounds.height)
        loadingView.addConstraint(loadingViewHeght!)
        loadingView.anchorWithConstantsToTop(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        self.mm_drawerController.setGestureCompletionBlock{
            (drawerController,gesture) -> Void in
            self.loadingView.isHidden=true
        }
        
        for i in 0...views.count-1 {
            let item=views[i]
            item.layer.borderWidth=1
            item.layer.borderColor=UIColor.gray.cgColor
            let gesture=CustomGesture(target: self, action: #selector(show_register(sender:)))
            gesture.tag=i
            item.addGestureRecognizer(gesture)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func open(_ sender: Any) {
        self.mm_drawerController.open(MMDrawerSide.left, animated: true, completion: {
            _ in
            self.loadingView.isHidden=false
        })
    }
    
    func show_register(sender:CustomGesture){
        if sender.tag==1{
            performSegue(withIdentifier: "second", sender: sender.tag)
        }else if sender.tag==0{
            performSegue(withIdentifier: "third", sender: 2)
        }else{
            performSegue(withIdentifier: "last", sender: 3)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        if segue.identifier=="second"{
            let dc=segue.destination as! SecondViewController
        }else if segue.identifier=="third"{
            let dc=segue.destination as! ThirdViewController
            dc.tag=2
        }else if segue.identifier=="last"{
            let dc=segue.destination as! LastTableViewController
            dc.tag=3
        }
    }
}
