//
//  RegisterViewController.swift
//  popular
//
//  Created by beka on 3/15/17.
//  Copyright © 2017 beka. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet var views: [UIView]!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title="register".localize(lang: lang)
        
        for i in 0...views.count-1 {
            let item=views[i]
            item.layer.borderWidth=1
            item.layer.borderColor=UIColor.gray.cgColor
            let gesture=CustomGesture(target: self, action: #selector(show_register(sender:)))
            gesture.tag=i
            item.addGestureRecognizer(gesture)
        }
    }

    func show_register(sender:CustomGesture){
        if sender.tag==0{
            UserDefaults.standard.setClient(true)
            performSegue(withIdentifier: "show", sender: sender.tag)
        }else{
            UserDefaults.standard.setClient(false)
            performSegue(withIdentifier: "show", sender: sender.tag)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        if segue.identifier=="show"{
        }
    }
}
class CustomGesture: UITapGestureRecognizer {
    var tag=0
}
