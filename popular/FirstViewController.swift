//
//  FirstViewController.swift
//  popular
//
//  Created by beka on 3/15/17.
//  Copyright © 2017 beka. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    @IBOutlet weak var kz_btn: UIButton!
    @IBOutlet weak var rus_btn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        kz_btn.layer.borderWidth=1
        kz_btn.layer.borderColor=UIColor(colorLiteralRed: 54/255, green: 175/255, blue: 188/255, alpha: 1).cgColor
        
        rus_btn.layer.borderWidth=1
        rus_btn.layer.borderColor=UIColor(colorLiteralRed: 54/255, green: 175/255, blue: 188/255, alpha: 1).cgColor
        
        kz_btn.addTarget(self, action: #selector(show_register(sender:)), for: .touchUpInside)
        rus_btn.addTarget(self, action: #selector(show_register(sender:)), for: .touchUpInside)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func show_register(sender:UIButton){
        if sender==kz_btn{
            UserDefaults.standard.setLang(value: "kz")
        }else{
            UserDefaults.standard.setLang(value: "rus")
        }
        performSegue(withIdentifier: "show", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier=="show"{
            lang=UserDefaults.standard.getLang()
        }
    }
}
