//
//  DetailTableViewController.swift
//  popular
//
//  Created by beka on 3/28/17.
//  Copyright © 2017 beka. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher

class DetailTableViewController: UITableViewController {

    var tag = -1
    var id=""
    @IBOutlet weak var header: UIView!
    @IBOutlet weak var img: UIImageView!
    var message=JSON.null
    var params=[String:String]()
    var loadingView:UIView={
        let l=UIView()
        l.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive=true
        l.backgroundColor=UIColor.black.withAlphaComponent(0.4)
        l.translatesAutoresizingMaskIntoConstraints=false
        let activity=UIActivityIndicatorView()
        activity.startAnimating()
        activity.activityIndicatorViewStyle = .whiteLarge
        activity.translatesAutoresizingMaskIntoConstraints=false
        l.addSubview(activity)
        activity.centerXAnchor.constraint(equalTo: l.centerXAnchor).isActive=true
        activity.centerYAnchor.constraint(equalTo: l.centerYAnchor).isActive=true
        l.isHidden=true
        return l
    }()
    var loadingViewHeght:NSLayoutConstraint?
    @IBOutlet weak var add: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(loadingView)
        loadingViewHeght=NSLayoutConstraint(item: loadingView, attribute:NSLayoutAttribute.height, relatedBy:NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: UIScreen.main.bounds.height)
        loadingView.addConstraint(loadingViewHeght!)
        loadingView.anchorWithConstantsToTop(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
 
        switch tag {
        case 0,1,-1:
             params["method"]="get"
            params["url"]="/api/List/Prepriatie?id=\(id)"
            header.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 200)
            break
        default:
            navigationItem.rightBarButtonItems=[]
             params["method"]="get"
            params["url"]="/api/List/Finansovaya_Organizacia?id=\(id)"
            header.frame=CGRect.zero
            break
        }
        
        self.tableView.estimatedRowHeight=50
        self.tableView.rowHeight=UITableViewAutomaticDimension
        load(params: params)
    }

    func load(params: [String : String]){
        loadingView.isHidden=false
        CustomRequests.request(params: params){
            response in
            self.loadingView.isHidden=true
            if response.result.error == nil{
                let result=JSON(response.result.value!)
                if result != JSON.null{
                    print(result)
                    self.message=result
                    if self.message["vladelec"].boolValue==false{
                        self.navigationItem.rightBarButtonItems=[]
                    }
                    if let img=self.message["photos"][0].string{
                        UIImageView().kf.setImage(with: URL.init(string: img), completionHandler: {
                            (image, error, cacheType, imageUrl) in
                            if image != nil{
                                self.img.image=image
                            }
                        })
                    }
                    self.tableView.reloadData()
                }
            }else{
                let cancel=UIAlertAction(title: "Закрыть", style: .destructive){
                    (result:UIAlertAction)->Void in
                    
                }
                self.showAlert(msg: "Произошла ошибка. Повторите попытку", actions: [cancel])
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tag==2{
            return 8
        }
        // #warning Incomplete implementation, return the number of rows
        return 6
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        switch indexPath.row {
        case 0:
            cell.textLabel?.text="Наименование компании"
            cell.detailTextLabel?.text=message["naimenovanie"].stringValue
            break
        case 1:
            if tag==2{
                cell.textLabel?.text="Область"
                cell.detailTextLabel?.text=message["oblast"].stringValue
            }else{
                cell.textLabel?.text="Вид переработки"
                cell.detailTextLabel?.text=message["vid_pererabotki"].stringValue
            }
            break
        case 2:
            if tag==2{
                cell.textLabel?.text="Вид услуги"
                cell.detailTextLabel?.text=message["vidi_uslug"].stringValue
            }else{
                cell.textLabel?.text="Цена по состоянию"
                cell.detailTextLabel?.text=message["cena"].stringValue
            }
            break
        case 3:
            if tag==2{
                cell.textLabel?.text="Годовая ставка"
                cell.detailTextLabel?.text=message["godovaya_stavka"].stringValue
            }else{
                cell.textLabel?.text="Количество сырья"
                cell.detailTextLabel?.text=message["kolvo_siria"].stringValue
            }
            break
        case 4:
            if tag==2{
                cell.textLabel?.text="Срок займа"
                cell.detailTextLabel?.text=message["srok"].stringValue
            }else{
                cell.textLabel?.text="Адрес"
                cell.detailTextLabel?.text=message["Adress"].stringValue
            }
            break
        case 5:
            if tag==2{
                cell.textLabel?.text="Способы погошения"
                cell.detailTextLabel?.text=message["sposob_pogashenia"].stringValue
            }else{
                cell.textLabel?.text="Характеристика"
                cell.detailTextLabel?.text=message["harakteristika"].stringValue
            }
            break
        case 6:
            cell.textLabel?.text="Аванс"
            cell.detailTextLabel?.text=message["avans"].stringValue
            break
        case 7:
            cell.textLabel?.text="Примечание"
            cell.detailTextLabel?.text=message["primichanie"].stringValue
            break
        default:
            break
        }
        
        return cell
    }
    
    
}
