//
//  AddViewController.swift
//  popular
//
//  Created by beka on 3/24/17.
//  Copyright © 2017 beka. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,AuthhDelegate{
    
    @IBOutlet weak var table: UITableView!
    var pererab_message=JSON.null
    var izdelie_message=JSON.null
    var loadingView:UIView={
        let l=UIView()
        l.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive=true
        l.backgroundColor=UIColor.black.withAlphaComponent(0.4)
        l.translatesAutoresizingMaskIntoConstraints=false
        let activity=UIActivityIndicatorView()
        activity.startAnimating()
        activity.activityIndicatorViewStyle = .whiteLarge
        activity.translatesAutoresizingMaskIntoConstraints=false
        l.addSubview(activity)
        activity.centerXAnchor.constraint(equalTo: l.centerXAnchor).isActive=true
        activity.centerYAnchor.constraint(equalTo: l.centerYAnchor).isActive=true
        l.isHidden=true
        return l
    }()
    var loadingViewHeght:NSLayoutConstraint?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title="add".localize(lang: lang)
        self.hideKeyboardWhenTappedAround()
        view.addSubview(loadingView)
        loadingViewHeght=NSLayoutConstraint(item: loadingView, attribute:NSLayoutAttribute.height, relatedBy:NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: UIScreen.main.bounds.height)
        loadingView.addConstraint(loadingViewHeght!)
        loadingView.anchorWithConstantsToTop(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddCell
        cell.delegate=self
        cell.parent=self
        cell.pererab_message=pererab_message
        cell.izdelie_message=izdelie_message
        for item in cell.tables{
            item.reloadData()
        }
        if cell.izdelie_message.count>0{
            cell.heights[1].constant=100
        }else if cell.pererab_message.count>0{
            cell.heights[0].constant=100
        }
        return cell
    }
    
    func load(params: [String : String]){
        loadingView.isHidden=false
        print(params)
        CustomRequests.request(params: params){
            response in
            self.loadingView.isHidden=true
            if response.result.error == nil{
                let result=JSON(response.result.value!)
                if params["url"]=="/api/List/Vid_pererabotki?vid=true" || params["url"]=="/api/List/Vid_pererabotki?vid=false"{
                    self.pererab_message=result
                    self.izdelie_message=JSON.null
                    self.table.reloadData()
                }else if params["url"]=="/api/Predpiatie/AddApplication"{
                    self.navigationController?.popViewController(animated: true)
                }
                else{
                    self.izdelie_message=result
                    self.table.reloadData()
                }
                
            }else{
                let cancel=UIAlertAction(title: "Закрыть", style: .destructive){
                    (result:UIAlertAction)->Void in
                    
                }
                self.showAlert(msg: "Произошла ошибка. Повторите попытку", actions: [cancel])
            }
        }
    }
    
    internal func request(params: [String : String]) {
        load(params: params)
    }
    
    internal func show() {
        
    }
    
    internal func scroll(point: CGPoint) {
        self.table.contentOffset.y=point.y
    }
    
    internal func dismiss() {
        self.view.endEditing(true)
        self.table.contentOffset.y=CGPoint.zero.y
    }
    
    
}
class AddCell: UITableViewCell,UITextFieldDelegate,UITextViewDelegate{
    @IBOutlet var views: [UIView]!
    @IBOutlet var labels: [UILabel]!
    @IBOutlet var tables: [UITableView]!
    @IBOutlet var heights: [NSLayoutConstraint]!
    @IBOutlet var fields: [UITextField]!
    @IBOutlet weak var textview: UITextView!
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var collection: UICollectionView!
    
    let toolBar:UIToolbar={
        let t=UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 30))
        return t
    }()
    var delegate:AuthhDelegate?
    var parent:UIViewController?
    
    var pots_params=[String:String]()
    var params=[String:String]()
    let vid_pere=["Животноводство","Растениеводство"]
    var pererab_message=JSON.null
    var izdelie_message=JSON.null
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        toolBar.items=[
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Закрыть", style: .plain, target: self, action: #selector(dismissKeyboard))
        ]
        
        for i in 0...views.count-1 {
            let item=views[i]
            item.layer.borderWidth=1
            item.layer.borderColor=UIColor.gray.cgColor
            let gesture=CustomGesture(target: self, action: #selector(toggle(sender:)))
            gesture.tag=i
            item.addGestureRecognizer(gesture)
        }
        
        for item in heights {
            item.constant=0
        }
        
        for i in 0...fields.count-1 {
            let item=fields[i]
            item.layer.borderWidth=1
            item.layer.borderColor=UIColor.gray.cgColor
            item.inputAccessoryView=toolBar
        }
        
        textview.layer.borderWidth=1
        textview.layer.borderColor=UIColor.gray.cgColor
        
        btn.setTitle("add".localize(lang: lang), for: .normal)
        btn.layer.borderWidth=1
        btn.layer.borderColor=UIColor.white.cgColor
        
        collection.backgroundColor=UIColor.clear
        collection.dataSource=self
        
        for item in tables {
            item.dataSource=self
            item.delegate=self
            item.estimatedRowHeight=50
            item.rowHeight=UITableViewAutomaticDimension
        }
    }
    
    func dismissKeyboard() {
        delegate?.dismiss()
    }
    
    func toggle(sender:CustomGesture){
        let height=heights[sender.tag]
        height.constant=height.constant==0 ? 100 : 0
    }
    
    @IBAction func add(_ sender: Any) {
        pots_params["method"]="upload"
        pots_params["url"]="/api/Predpiatie/AddApplication"
        delegate?.request!(params: pots_params)
    }
    
    func cell1_select(sender:CustomGesture){
        let height=heights[0]
        height.constant=0
        let label=labels[0]
        label.textColor=UIColor.black
        label.text=pererab_message[sender.tag]["name"].stringValue
        params["method"]="get"
        params["url"]="/api/List/Vid_izdelii?pererabotkaid=\(pererab_message[sender.tag]["Id"].stringValue)"
        pots_params["vid_pererabotkiid"]=pererab_message[sender.tag]["Id"].stringValue
        delegate?.request!(params:params)
    }
    
    func cell2_select(sender:CustomGesture){
        let height=heights[1]
        height.constant=0
        let label=labels[1]
        label.textColor=UIColor.black
        label.text=izdelie_message[sender.tag]["name"].stringValue
        pots_params["vid_izdeliid"]=izdelie_message[sender.tag]["Id"].stringValue
    }
    
    func cell3_select(sender:CustomGesture){
        let height=heights[2]
        height.constant=0
        let label=labels[2]
        label.textColor=UIColor.black
        label.text=vid_pere[sender.tag]
        params["method"]="get"
        if sender.tag==0{
            params["url"]="/api/List/Vid_pererabotki?vid=true"
            pots_params["vid"]="true"
        }else{
            params["url"]="/api/List/Vid_pererabotki?vid=false"
            pots_params["vid"]="false"
        }
        delegate?.request!(params:params)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.scroll(point: textField.frame.origin)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField==fields[0]{
            if let text=textField.text{
                pots_params["kolvo_siria"]=text
            }
        }else if textField==fields[1]{
            if let text=textField.text{
                pots_params["cena"]=text
            }
        }else if textField==fields[2]{
            if let text=textField.text{
                pots_params["naimenovanie"]=text
            }
        }else if textField==fields[3]{
            if let text=textField.text{
                pots_params["Adress"]=text
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField==fields[0]{
            if let text=textField.text{
                pots_params["kolvo_siria"]=text
            }
        }else if textField==fields[1]{
            if let text=textField.text{
                pots_params["cena"]=text
            }
        }else if textField==fields[2]{
            if let text=textField.text{
                pots_params["naimenovanie"]=text
            }
        }else if textField==fields[3]{
            if let text=textField.text{
                pots_params["Adress"]=text
            }
        }
        delegate?.dismiss()
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        delegate?.scroll(point: textView.frame.origin)
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        if let text=textView.text{
            pots_params["harakteristika"]=text
        }
        delegate?.dismiss()
        return true
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if let text=textView.text{
            pots_params["harakteristika"]=text
        }
    }
}
extension AddCell:UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell=collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! AddCollectionCell
        cell.layer.cornerRadius=5
        cell.layer.borderColor=UIColor.gray.cgColor
        cell.layer.borderWidth=1
        cell.parent=parent
        cell.active_btn.tag=indexPath.row
        return cell
    }
}
var photos=[UIImage(),UIImage(),UIImage(),UIImage(),UIImage()]
class AddCollectionCell: UICollectionViewCell,UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    var parent:UIViewController?
    var imagePicker = UIImagePickerController()
    var active_btn=UIButton()
    @IBAction func select_photo(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){

            active_btn=sender
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
            imagePicker.allowsEditing = false
            
            parent?.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        let img=info[UIImagePickerControllerOriginalImage] as? UIImage
        if img != nil{
            active_btn.setImage(img, for: .normal)
            photos[active_btn.tag]=img!
        }
    }
}
extension AddCell:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView==tables[2]{
            return vid_pere.count
        }else if tableView==tables[0]{
            return pererab_message.count
        }
        return izdelie_message.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView==tables[0]{
            let cell=tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath)
            cell.textLabel?.text=pererab_message[indexPath.row]["name"].stringValue
            let gesture=CustomGesture(target: self, action: #selector(cell1_select(sender:)))
            gesture.tag=indexPath.row
            cell.addGestureRecognizer(gesture)
            return cell
        }else if tableView==tables[2]{
            let cell=tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath)
            cell.textLabel?.text=vid_pere[indexPath.row]
            let gesture=CustomGesture(target: self, action: #selector(cell3_select(sender:)))
            gesture.tag=indexPath.row
            cell.addGestureRecognizer(gesture)
            return cell
        }
        let cell=tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath)
        cell.textLabel?.text=izdelie_message[indexPath.row]["name"].stringValue
        let gesture=CustomGesture(target: self, action: #selector(cell2_select(sender:)))
        gesture.tag=indexPath.row
        cell.addGestureRecognizer(gesture)
        return cell
    }
}
