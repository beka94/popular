//
//  ForgetViewController.swift
//  popular
//
//  Created by beka on 3/28/17.
//  Copyright © 2017 beka. All rights reserved.
//

import UIKit

class ForgetViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,AuthhDelegate {

    var t="change"
    @IBOutlet weak var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title=t.localize(lang: lang)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    internal func show() {
      
    }
    
    internal func scroll(point: CGPoint) {
        self.table.contentOffset.y=point.y
    }
    
    internal func dismiss() {
        self.view.endEditing(true)
        self.table.contentOffset.y=CGPoint.zero.y
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if t == "change"{
            let cell=tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! ForgetCell
            cell.delegate=self
            cell.isChange=true
            return cell
        }
        let cell=tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if t == "change"{
            return 200
        }
        return 100
    }
}
class ForgetCell: UITableViewCell,UITextFieldDelegate {
    @IBOutlet var views: [UITextField]!
    var delegate:AuthhDelegate?
    var isChange:Bool?{
        didSet{
            if isChange!{
                for i in 0...views.count-1 {
                    let item=views[i]
                    item.layer.borderWidth=1
                    item.layer.borderColor=UIColor(colorLiteralRed: 6/255, green: 112/255, blue: 122/255, alpha: 1).cgColor
                }
            }else{
                
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.scroll(point: textField.frame.origin)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        delegate?.dismiss()
        return true
    }
}
