//
//  SecondViewController.swift
//  popular
//
//  Created by beka on 3/28/17.
//  Copyright © 2017 beka. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet var views: [UIView]!
    override func viewDidLoad() {
        super.viewDidLoad()

        for i in 0...views.count-1 {
            let item=views[i]
            item.layer.borderWidth=1
            item.layer.borderColor=UIColor.gray.cgColor
            let gesture=CustomGesture(target: self, action: #selector(show_register(sender:)))
            gesture.tag=i
            item.addGestureRecognizer(gesture)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func show_register(sender:CustomGesture){
        performSegue(withIdentifier: "show", sender: sender.tag)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        if segue.identifier=="show"{
            let dc=segue.destination as! ThirdViewController
            dc.tag=sender as! Int
        }
    }
}
